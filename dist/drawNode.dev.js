"use strict";

function drawnode(node) {
  console.log('drawnode node:', node); // Create Elements

  newNodeElement = document.createElement('div');
  newNodeElement.style.top = 60;
  newNodeElement.style.left = 60;
  $(newNodeElement).attr('id', node.id);
  var containerId = $(newNodeElement).attr('id');
  $(newNodeElement).addClass('container');
  $(newNodeElement).addClass(node.type);
  dragzone = document.createElement('div');
  deleteElement = document.createElement('div');
  var deleteIconElement = document.createElement('img');
  datadiv = document.createElement('div');
  datadiv.style.display = 'flex';
  datadiv.style.flexDirection = 'column';
  datadiv.style.alignItems = 'center';
  datadiv.style.justifyContent = 'center';
  labelinput = document.createElement('div');
  descriptionAndInput = document.createElement('div');
  var br = document.createElement('br'); // Set Node postion

  if (node.top == '') {} else {
    console.log('set position');
    $(newNodeElement).css({
      top: node.top,
      left: node.left
    });
  } // Append New Node to Canvas


  $('#canvasdiv').append(newNodeElement);
  deleteIconElement.setAttribute('src', ' icon-error.png');
  $(deleteIconElement).uniqueId();
  $(deleteIconElement).attr('align', 'right');
  $(deleteIconElement).attr('width', '15px');
  $(deleteElement).addClass('delete');
  $(deleteElement).append(deleteIconElement);
  $(deleteElement).addClass('delete');
  $(dragzone).uniqueId();
  var deleteId = $(deleteIconElement).attr('id');
  var currentId = $(dragzone).attr('id');

  if (mode == 'student') {
    $(newNodeElement).append(deleteElement);
  }

  $(newNodeElement).append(dragzone);
  addShape(node.type, dragzone, node.color, node.outlinecolor);
  $(datadiv).addClass('datatable');
  $(datadiv).addClass('input-group');
  $(newNodeElement).append(datadiv);
  var prob;
  var problabel;
  var probs;
  var nodeType = node.type;
  var labelText = nodeType === 'C' ? 'FR :' : 'Q :';
  console.log(node);

  if (node.bordercolor == 'red') {
    prob = addtextwithred('Prob', node);
  } else if (node.bordercolor == 'blue') {
    prob = addtextwithblue('Prob', node);
  } else {
    prob = addtext('Prob', node);
  }

  problabel = addlabel(labelText);
  var description = addtext('Description', node, {
    width: '999rem',
    position: '-100px',
    display: 'float'
  });
  $(descriptionAndInput).append(description);
  descriptionAndInput.style.display = 'flex';
  descriptionAndInput.style.height = '0px';
  $(datadiv).append(descriptionAndInput);
  $(labelinput).append(problabel);
  $(labelinput).append(prob);
  labelinput.style.display = 'flex';
  $(datadiv).append(labelinput); // $(datadiv).append(dropL.show())
  //$(datadiv).append(br);
  // $(datadiv).append(problabel);
  //  $(datadiv).append(prob);
  //  $(datadiv).append(probs);
  // Add input Event Linstener

  prob.addEventListener('input', function (e) {
    var num = prob.value;
    var message = validateProbability(num);

    if (message != 'true') {
      alert(message);
      prob.value = prob.defaultValue;
    } else {
      prob.defaultValue = num;
    }
  }, false);
  $(description).change(function () {
    node.description = $(description).val();
    updateNode(description, 'description');
  });
  $(description).focusin(function () {
    description.style.width = '100rem';
    description.style.backgroundColor = '#fff';
    description.style.opacity = 1;
    description.style.zIndex = 9999999999999999;
  });
  $(description).focusout(function () {
    description.style.width = '8rem';
  }); // Add change Event for probability input text

  $(prob).change(function () {
    node.prob = $(prob).val();
    updateNode(node, 'prob');
  });
  /*   $(emv).change(function() {
            node.emv= $(emv).val();
            updateNode(node,"emv");
          
       });  
    */
  // Add Delete Event

  $('#' + deleteId).click(function () {
    if (confirm('Delete this node?')) {
      jsPlumb.detachAllConnections(currentId);
      jsPlumb.removeAllEndpoints(currentId);
      $('#' + node.id).empty();
      deleteNode(node);
      $(this).empty();
      sentToparentPage();
    }
  });
  jsPlumb.draggable($('#' + containerId), {
    containment: $('#canvasdiv').parent(),
    scroll: false
  });
  $('#' + containerId).draggable({
    containment: $('#canvasdiv').parent(),
    scroll: false,
    handle: '#' + currentId,
    stop: function stop(event, ui) {
      console.log('event:', event);
      console.log('ui:', ui);
      position = ui.position; //value="top:"+position.top+"left:"+position.left;

      console.log('top:' + position.top + 'left:' + position.left);
      node.top = position.top;
      node.left = position.left;
      updateNode(node, 'top');
      updateNode(node, 'left');
    }
  }); //connection

  var top = $('#' + containerId).position().top;
  var left = $('#' + containerId).position().left;
  node.top = top;
  node.left = left;
  return node;
}