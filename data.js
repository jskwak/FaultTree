function addDroplist(options) {
	newselect = document.createElement('select')
	$(newselect).addClass('droplist')
	var select = $(newselect).uniqueId()
	selectId = $(newselect).prop('id')
	for (o = 0; o < op.length; o++) {
		$(newselect).append('<option value=' + o + ' style="width:100;">' + op[o] + '</option>')
	}

	if (typeof options == 'undefined') {
		return $(newselect)
	} else {
		newselect.value = options
		return $(newselect)
	}
}

function addlabel(label) {
	var newlabel = document.createElement('LABEL')
	newlabel.setAttribute('for', label)
	newlabel.style.width = '100%'
	newlabel.innerHTML = label
	return newlabel
}

function addtext(label, node, style) {
	var text = document.createElement('input')
	text.type = 'text'
	text.name = label
	if (style) text.style = style
	else text.style.width = '7rem'

	text.style.backgroundColor = '#ffffff4f'

	$(text).uniqueId()
	$(text).addClass('droplist')

	$(text).addClass('form-control')

	//text.style.borderColor = "red";

	if (label === 'Prob') {
		if (node.prob) {
			text.value = node.prob
		}
	} else if (label === 'Description') {
		if (node.description) {
			text.value = node.description
		}
	} else {
		if (node) text.value = node
	}

	return text
}

function addtextwithred(label, node) {
	return this.addtext(label, node, { boderColor: 'red', width: '7rem' })
}

function addtextwithblue(label, node) {
	return this.addtext(label, node, { boderColor: '#102bde', width: '7rem' })
}
